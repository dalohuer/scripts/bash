#!/bin/bash
# Comprobar que el disco duro este conectado a la maquina virtual.
#mount -t ntfs-3g /dev/sdb1 /media/usb
month=`date +"%m"`
month=$((month-1))
echo $month
year=`date +"%Y"`
recorddir="/var/spool/asterisk/monitor/$year/$month"
backupdir="/media/usb/grabaciones/$year/$month"
mkdir -p $backupdir
for i in `ls $recorddir`
do
        echo "dia #$i"
        cd $recorddir/$i
        mkdir -vp $backupdir/$i
        for file in *.wav; do
                mp3=$(basename "$file" .wav).mp3;i
                nice lame -b 16 -m m -q 9-resample "$file" "$mp3";
                mv "$mp3" $backupdir/$i;
        done
done

