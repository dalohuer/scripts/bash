#!/bin/bash

##############################################################################
#                                                                            #
#	   _   ___                     ____    __    __   __       __        #
#	  | | / (_)__ ___  ___  ___   / __/__ / /__ / /__/ /____ _/ /        #
#	  | |/ / (_-</ _ \/ _ \/ -_) _\ \/ -_) / -_)  '_/ __/ _ `/ _ \       #
#	  |___/_/___/\___/_//_/\__/ /___/\__/_/\__/_/\_\\__/\_,_/_//_/       #
#								             #
#				                                             #
#		__  __          __       __         __  __  ___              #
#		\ \/ /__  __ __/ /___ __/ /  ___ __/ /_/  |/  /__ _  __      #
#		 \  / _ \/ // / __/ // / _ \/ -_)_  __/ /|_/ / _ \ |/ /      #
#		 /_/\___/\_,_/\__/\_,_/_.__/\__/ /_/ /_/  /_/ .__/___/       #
#				                                   /_/       #
#				    	   ____        _      __             #
#				    	  / __/_______(_)__  / /_            #
#				    	 _\ \/ __/ __/ / _ \/ __/            #
#				    	/___/\__/_/ /_/ .__/\__/             #
#                                                    /_/                     #
#                                                                            #
#                                                                            #
#                                                                            #
##############################################################################

# This script is open-source so you can make whatever you want with it
#
# This script need some dependencies:
#  1. MPV 
#  2. Youtube-dl 
#  3. Xclip
#  4. Rofi
#  5. Dmenu
#
# This script allow you to download or play videos of all sites compatibles with youtube-dl.
# More info about youtube-dl in https://github.com/ytdl-org/youtube-dl
#
# Configuration
#
#   1. Terminal
#  		Edit the line: term="termite" and change it for any teminal you use
#
#   2. Directory
#		Edit the lines:
#			dr=/home/$USER/ytdl-mpv/
#			mkdir /home/$USER/ytdl-mpv
#       Change them to any path or dir you want all downloads be store
# 
#   3. Menu format
#		Edit the lines with:
#			rofi -dmenu -theme Pop-Dark
#		Change it to any format, theme you want
#




# Setting Terminal
term="termite"

# Using xplit to input video
url=$(xclip -o -select c)

# Path to Downloads
dr=/home/$USER/ytdl-mpv/
if [ -d "$dr" ]
	then
		cd $dr
	else
		mkdir /home/$USER/ytdl-mpv
		cd $dr
fi

# Menu Options
	funcMO() {
		options="DownloadMenu\nPlayMenu"
		mo=$(echo -e "$options" | rofi -dmenu -p  " Menu " )

		case "$mo" in
			DownloadMenu) funcDM ;;
			PlayMenu) funcPM ;;
		esac
	}


# Download Menu func
funcDM() {
	options="DownloadVideo\nDownloadAudio\nDownloadSubs\nDownloadPlaylist"
	dm=$(echo -e "$options" | rofi -dmenu -p " Descargar Menu")

	case "$dm" in 
		DownloadVideo) funcDVideo ;;
		DownloadAudio) funcDAudio ;;
		DownloadSubs) funcDSubs ;;
		DownloadPlaylist) funcDPlaylist ;;
	esac
}

# Play Menu func
funcPM() {
	options="PlayVideo\nPlayAudio\nPlayPlaylist"
	pm=$(echo -e "$options" | rofi -dmenu  -p " PlayMenu" )

	case "$pm" in
		PlayVideo) funcPVideo  ;;
		PlayAudio) mpv -ytdl-format="bestaudio" $url ;;
		PlayPlaylist) funcPPLM ;;
	esac
}


# func Download Video

funcDVideo() {
	options="1.BestQualityVideoAudio\n2.Quality1080p\n3.Quality720p\n4.Quality480p\n5.ChooseQuality\n6.Exit"
	dv=$( echo -e "$options" | rofi -dmenu  -p " Descargar Video" )

	case "$dv" in

		1.BestQualityVideoAudio) 
			"$term" -e  "youtube-dl -f bestvideo+bestaudio/best $url " ;
			funcOtherDVideo ;;

		2.Quality1080p) 
			 "$term" -e "youtube-dl -f bestvideo[height=1080]+bestaudio/best[height=1080] $url" ; 
			 funcOtherDvideo ;;

		3.Quality720p)
			"$term" -e "youtube-dl -f bestvideo[height=720]+bestaudio/best[height=720] $url" ; 
			funcOtherDvideo ;;

		4.Quality480p)
			"$term" -e "youtube-dl -f bestvideo[height=480]+bestaudio/best[height=480] $url" ; 
			funcOtherDvideo ;;

		5.ChooseQuality) funcDCQ ;;

		6.Exit) exit ;;
	esac
}

# func Choose Quality Download Video

funcDCQ() {
	
	options=$(youtube-dl --list-formats "$url" | grep -v "audio only" | awk 'NR==4 , NR==end {print $1 "-" $2 "-" $5}' | rofi -dmenu -p "Elige calidad")
	qy=$(echo -e "$options" | awk '{print $1}')
	mpv -ytdl "$qy" "$url"
	funcOtherDVideo
}


# func Download Audio

funcDAudio(){
	options="1.BestQualityMP3\n2.ChooseQualityBitrate\n3.ChooseBetweenAvailablesFormats\n4.Exit"
	da=$(echo -e "$options" | rofi -dmenu  -p " Descargar Audio")

	case "$da" in
		1.BestQualityMP3)
			"$term" -e "youtube-dl --extract-audio --audio-quality 0 --audio-format mp3 $url" ;  
			funcOtherDAudio ;;

		2.ChooseQualityBitrate) funcDQB ;;

		3.ChooseBetweenAvailablesFormats) funcCBAF  ;;
		
		4.Exit) exit ;;
	esac
		
}

# func Choose Quality Bitrate

funcCQB() {
	br=$( rofi -dmenu  -p "Choose Bitrate between 0-9 / 0=best 9=worst")
	"$term" -e "youtube-dl --extract-audio --audio-quality "$br" --audio-format mp3"
	funcOtherDAudio 
}

# func Choose Between Availables Formats

funcCBAF() {
	format=$(youtube-dl --list-formats $url | grep "audio only" | awk '{print $1 "-" $2 "-" $6}' |  rofi -dmenu  -p " Posiles Formatos " )
	fm=$(echo -e "$format" | awk '{print $1}')
	"$term" -e "youtube-dl "$fm" $url"
	funcotherDAudio
}


# func Download Subtitles menu

funcDSubs() {
	options="OriginalSubs\nSubsFormatLanguage\nAllSubs\nExit"
	sm=$( echo -e "$options" | rofi -dmenu  -p "Descargar Subs Menu")

	case "$sm" in
		OriginalSubs)
			"$term" -e "youtube-dl --write-sub --skip-download $url" ; 
			funcOtherDSub ;;
			
		SubsFormatLanguage) funcSFL ;;
		
		AllSubs) "$term" -e "youtube-dl --all-subs --skip-download $url" ; 
			 funcOtherDSub ;;
		
		Exit) exit ;;
	esac
}

# func Choose Subs Format

funcSFL() {
	opt="SRT\nASS"
	fopt=$(echo -e "$opt" | rofi -dmenu  -p " Elige Formato Subs ")
	opt1="ES\nEn\nFR\nITA\nGER"
	lopt=$(echo -e "$opt1" | rofi -dmenu  -p " Elige Lang Subs")
	"$term" -e "youtube-dl --sub-format $fopt --sub-lang $lopt --skip-download $url" ;
	funcOtherDSub
}

# func Download PlayList

funcDPlaylist() {
	options="VideoAudio\nAudio\nByDate\nExit"
	dp=$(echo -e "$options" | rofi -dmenu  -p " PlayList Menu")

	case "$dp" in
		VideoAudio)funcDVideo ; 
			funcOtherDPlaylist ;;
		Audio) funcDAudio ; 
			funcOtherDAudio ;;
		ByDate) funcPPLBD ;;
		Exit) exit ;;
	esac
}

# func Download Other Video

funcOtherDVideo() {
	options="DownloadOtherVideo\nExit"
	odv=$(echo -e "$options" | rofi -dmenu  -p "Exit Menu")

	case "$odv" in
		DownloadOtherVideo) funcDVideo ;;
		Exit) funcMO ;;
	esac
}

# func Download Other Audio

funcOtherDAudio() {
	options="DownloadOtherAudio\nExit"
	oda=$(echo -e "$options" | rofi -dmenu  -p "Exit Menu")

	case "$oda" in
		DownloadOtherAudio) funcDAudio ;;
		Exit) funcMO ;;
	esac
}

# func Download Other Sub

funcOtherDSub() {
	options="DownloadOtherSub\nExit"
	ods=$(echo -e "$options" | rofi -dmenu  -p "Exit Menu")

	case "$ods" in
		DownloadOtherSub) funcDSub ;;
		Exit) funcMO ;;
	esac
}

funcPVideo() {
	options="1.BestQualityVideoAudio\n2.Quality1080p\n3.Quality720p\n4.Quality480p\n5.ChooseQuality\n6.Exit"
	dv=$( echo -e "$options" | rofi -dmenu  -p " Descargar Video" )

	case "$dv" in

		1.BestQualityVideoAudio) 
			mpv -ytdl-format="bestvideo+bestaudio/best" "$url" ;  
			funcOtherPVideo ;;

		2.Quality1080p) 
			mpv -ytdl-format="bestvideo[height=1080]+bestaudio/best[height=1080]" "$url" ; 
			funcOtherPvideo ;;

		3.Quality720p)
			mpv -ytdl-format="bestvideo[height=720]+bestaudio/best[height=720]" "$url" ; 
			funcOtherPvideo ;;

		4.Quality480p)
			mpv -ytdl-format="bestvideo[height=480]+bestaudio/best[height=480]" "$url" ; 
			funcOtherPvideo ;;

		5.ChooseQuality) funcPCQ ;;

		6.Exit) exit ;;
	esac
}

funcOtherPVideo() {
	options="PlayOtherVideo\nExit"
	odv=$(echo -e "$options" | rofi -dmenu  -p "Exit Menu")

	case "$odv" in
		PlayOtherVideo) funcPVideo ;;
		Exit) funcMO ;;
	esac
}


# func Choose Quality Play Video

funcPCQ() {
	options=$(youtube-dl --list-formats "$url" | grep -v "audio only" | awk 'NR==4 , NR==end {print $1 "-" $2 "-" $5}' | rofi -dmenu -p "Elige calidad")
	qy=$(echo -e "$options" | awk '{print $1}')
	mpv -ytdl "$qy" "$url" ;
	funcOtherPVideo
}	


# func Play Playlist

funcPPLM() {
	options="VideoAudio\nAudio"
	ppl=$(echo -e "$options" | rofi -dmenu  -p "PlayList Menu")

	case "$ppl" in
		VideoAudio) mpv -ytdl-format="bestvideo+bestaudio/best" $url ;;
		Audio) mpv --ytdl-format="bestaudio" $url ;;
	esac
}

# func Play Playlist by Date

funcPPLBD() {
	dt=$( rofi -dmenu  -p "FECHA=YYYMMDD / Elige fecha desde la que empezar a descargar" | awk '{print $1}' )
	mpv ytdl-date="$dt" "$url"
	funcOtherPlDate
}

funcMO
