
#!/bin/bash

DST="/home/soporte/records"

#if [ ! -z "$(ls $DST)" ]; then
#    rm -r $DST/*
#fi

rm -r $DST/*

#rm -r /home/soporte/records/*

read -p "Introduzca el Número a buscar: " NUM
find /var/grabaciones/ -name "*$NUM*" | grep 2021- >  files
 
for i in $(cat files); do
    FECHA=$( echo $i|awk -F'/' '{print $5}' )
if [ ! -d "$DST/$FECHA" ]; then
    mkdir $DST/$FECHA
fi
    cp -v $i $DST/$FECHA/
done
 
echo "Proceso terminado!!"
echo "Ya puede descargar los archivos de la carpeta records!!"
