echo "Por favor comprobar que el disco duro este conectado a la maquina virtual."
echo "si no está conectado por favor conectarlo y montarlo con el siguiente comando"
echo "mount -t ntfs-3g /dev/sdb1 /media/usb"
sleep 3
ORIGEN="/var/spool/asterisk/monitor"
DESTINO="/media/usb/grabaciones"
# ORIGEN="/home/sinet/Borrar/grabaciones"
# DESTINO="/home/sinet/grabaciones"

read -p "Número del mes y año a respaldar ej: "2021/01" : " FECHA
echo "FECHA"
recorddir="$ORIGEN/$FECHA"
backupdir="$DESTINO/$FECHA"
for i in `ls $recorddir`; do
        echo "dia #$i"
        cd $recorddir/$i
        mkdir -p $backupdir/$i
        for file in *.gsm; do
                cp -ruv *.gsm $backupdir/$i;
        done
done
