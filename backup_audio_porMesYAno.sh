# Comprobar que el disco duro este conectado a la maquina virtual.
#mount -t ntfs-3g /dev/sdb1 /media/usb
read -p "Número del mes a respaldar ej: "01" : " MES
read -p "Número del año a respaldar ej: "2020" : " ANO
month=$MES
echo $month
year=$ANO
recorddir="/var/spool/asterisk/monitor/$year/$month"
backupdir="/media/usb/grabaciones/$year/$month"
mkdir -p $backupdir
for i in `ls $recorddir`
do
        echo "dia #$i"
        cd $recorddir/$i
        mkdir -vp $backupdir/$i
        for file in *.wav; do
                mp3=$(basename "$file" .wav).mp3;i
                nice lame -b 16 -m m -q 9-resample "$file" "$mp3";
                mv "$mp3" $backupdir/$i;
        done
done
